#packages de gestion de données
import numpy as np
import pandas as pd

#package de visualisation
import matplotlib.pyplot as plt

#packages de modélisation
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.datasets import make_moons, load_breast_cancer
from sklearn.model_selection import train_test_split

#noise = None pour la partie trainin
data = make_moons(n_samples = 100, shuffle = True, noise=0.08, random_state=123)

#Partie: Training
#separation des vars explicatives de la var cible
#X_train = np.array(data[0])
#Y_train = np.array(data[1])

#Visuel du jeu de données

#plt.scatter(X_train[:, 0],X_train[:, 1], c= Y_train)
#plt.show()
#


#partie test avec un arbre de decision

X_test = np.array(data[0])
Y_test =np.array(data[1])


model_example = DecisionTreeClassifier(max_depth=4)
model_example.fit(X_test,Y_test)

Y_test_pred = model_example.predict(X_test)

#plt.scatter(X_test[:,0],X_test[:,1],c = Y_test_pred)
#plt.show()

#on observe qu'il ya des pointillés dans le jeu de données initiales qui sont en réalité jaune, avec l'abre de décision certains points
# on été classifiés violet alors qu'en réalit ils sont jaunes(et inversement)

#Comment le visualiser ?

n_classes = 2 # car on a deux modalités
plot_colors = "ryb"
plot_step = 0.02

x_min ,x_max = X_test[:, 0].min() - 1,X_test[:,0].max() + 1
y_min , y_max = X_test[:,0].min() - 1,X_test[:,0].max() + 1
xx, yy = np.meshgrid(np.arange(x_min,x_max, plot_step),np.arange(y_min,y_max,plot_step))
plt.tight_layout(h_pad=0.5,w_pad=0.5)

Z1 = model_example.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z1.reshape(xx.shape)
cs = plt.contourf(xx,yy,Z,cmap = plt.cm.RdYlBu)

plt.xlabel("X1")
plt.ylabel("X2")

#Visualiser les points
for i, color in zip(range(n_classes),plot_colors):
    idx = np.where(Y_test == i)
    plt.scatter(X_test[idx,0],X_test[idx,1], c = color , cmap=plt.cm.RdYlBu,edgecolors='black', s = 15)


#plt.show()

#afficher les règles de décision
plot_tree(model_example)
plt.show()


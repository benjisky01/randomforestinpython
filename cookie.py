
from sklearn.neural_network import MLPClassifier
import numpy as np 

#sweet  = 1
# bitter  = 0

obs = np.array([
    [1,0,1],
    [1,0,1],
    [0,1,0],
    [1,0,1],
    [1,0,1],
    [0,1,0],
    [1,0,1],
    [0,1,0],
])


features_train = obs[:, 0:2]
labels_train = obs[:, 2]

test = np.array([
    [1, 0, 1],
    [1, 0, 1],
    [0, 1, 0],
    [0, 1, 0],
])

features_test = test[:, 0:2]
labels_test = test[:, 2]


# on créer le model

mlp = MLPClassifier(hidden_layer_sizes=5,
max_iter=3000)

#Dans notre RNA, on dit qu'il ya 5 couches cachées
#Notre modèle va passer 3000 fois par les données

#ensuite on va tester les données d'entrain


mlp.fit(features_test,labels_test)

print(f"Traininng set score :{mlp.score(features_train,labels_train)}")
print(f"Testing set score: {mlp.score(features_test, labels_test)}\n")

features_list=[
    {"features": [[1, 0]], "type": "Sweet cookie"},
    {"features": [[0, 1]], "type": "Bitter cookie"}
]

for i in features_list:
    print(f"type:{i['type']}")
    if (mlp.predict(i['features']) == 1):
        print("good cookie")
    else:
        print("bad cookie")